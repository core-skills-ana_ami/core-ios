//
//  UserDefaultsViewController.swift
//  Core Skills
//
//  Created by Gratch on 3/11/20.
//  Copyright © 2020 Jonathan Gratch. All rights reserved.
//
//  
//  Assignment Notes: Much like the CoreData feature, the save and load
//  buttons should save the TextFields and load into the Labels.
//  Plenty of example code out there, but here's one to start with:
//  http://www.thomashanning.com/userdefaults/

import UIKit

class UserDefaultsViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var compIDTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var compIDLabel: UILabel!
    
    @IBAction func saveData(_ sender: UIButton) {
        UserDefaults.standard.set( true, forKey: "Name Text")
        UserDefaults.standard.set(true, forKey: "Text Field")
        UserDefaults.standard.set(true, forKey: "Name Label")
        UserDefaults.standard.set(true, forKey: "ID Label")
        
        // Add your code here
    }
    @IBAction func loadData(_ sender: UIButton) {
        nameTextField = UserDefaults.standard.string(forKey: "Text Name") as? UITextField
        compIDTextField = UserDefaults.standard.string(forKey: "Text Field") as? UITextField
        nameLabel = UserDefaults.standard.string(forKey: "Name Label") as? UILabel
        

        
        
        // Add your code here
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UserDefaultsViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
