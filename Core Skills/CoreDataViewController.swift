//
//  CoreDataViewController.swift
//  Core Skills
//
//  Created by Gratch on 3/11/20.
//  Copyright © 2020 Jonathan Gratch. All rights reserved.
//
//
//  Assignment Notes: We have already provided the data model for
//  you to use.  Make sure to look at CoreData.xcdatamodeld to see
//  how things are being stored.  Your two buttons here should 1) save
//  the data from the TextFields to CoreData and 2) load whatever is
//  the last entry in CoreData into the Labels.  I would look here for info:
//  https://www.raywenderlich.com/145809/getting-started-core-data-tutorial

import UIKit
import CoreData

class CoreDataViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var compIDTextField: UITextField!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var compIDLabel: UILabel!
    
    @IBAction func saveData(_ sender: UIButton) {
        guard let appDelegate =

          UIApplication.shared.delegate as? AppDelegate else {

          return

        }

        // 1

        let managedContext =

          appDelegate.persistentContainer.viewContext

        

        // 2

        let entity =

          NSEntityDescription.entity(forEntityName: "Person",

                                     in: managedContext)!

        let entity2 =

          NSEntityDescription.entity(forEntityName: "ID",

                                     in: managedContext)!

        

        let person = NSManagedObject(entity: entity,

                                     insertInto: managedContext)

        let compID = NSManagedObject(entity: entity2,

                                     insertInto: managedContext)

        

        // 3

        person.setValue(nameLabel.text, forKeyPath: "name")

        compID.setValue(compIDLabel.text, forKeyPath: "ID")

        

        // 4

        do {

          try managedContext.save()

        } catch let error as NSError {

          print("Could not save. \(error), \(error.userInfo)")

        }

        
    }
    
    
    @IBAction func loadData(_ sender: UIButton) {
         let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext



               let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Student")



               request.returnsObjectsAsFaults = false

               do {

                   let result = try context.fetch(request)

                   for data in result as! [NSManagedObject] {

                      print(data.value(forKey: "name") as! String)

                 }



               } catch {



                   print("Failed")

               }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CoreDataViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
   
}
