//
//  AccelerometerViewController.swift
//  Core Skills
//
//  Created by Gratch on 3/11/20.
//  Copyright © 2020 Jonathan Gratch. All rights reserved.
//
//  Assignment Notes: To make this simpler and to not have
//  to deal with UI updates, your code in startAccel should
//  only update the xDir, yDir, and zDir Double values whenever
//  the accelerometer changes.  checkDeviceRotation will copy
//  the current values of the variables to the labels when
//  called.

import UIKit
import CoreMotion

class AccelerometerViewController: UIViewController {
    
    // NOTE: This can only really be tested on a device, not the simulator!
    
    lazy var motionManager = CMMotionManager()
    var xDir: Double!
    var yDir: Double!
    var zDir: Double!
    
    @IBOutlet weak var xLabel: UILabel!
    @IBOutlet weak var yLabel: UILabel!
    @IBOutlet weak var zLabel: UILabel!
    
    @IBAction func startAccel(sender: UIButton) {
        // Add your code here
        if motionManager.isAccelerometerAvailable{
            let queue = OperationQueue()

        motionManager.startAccelerometerUpdates(to: queue, withHandler:

            {data, error in
                guard let data = data else{
                    return
                }
                print("X = \(data.acceleration.x)")

                print("Y = \(data.acceleration.y)")

                print("Z = \(data.acceleration.z)")

                self.xDir = data.acceleration.x

                self.yDir = data.acceleration.y

                self.zDir = data.acceleration.z
            }

        )

        } else {

            print("Accelerometer is not available")

        }
    }
    
    @IBAction func checkDeviceRotation(sender: UIButton) {
        // Helper function to update the screen.  No edits needed.
        xLabel.text = "X = " + String(xDir)
        yLabel.text = "Y = " + String(yDir)
        zLabel.text = "Z = " + String(zDir)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
}
